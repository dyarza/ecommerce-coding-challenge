package com.ecommerce.challenge.rest;

import com.ecommerce.challenge.rest.model.Cart;
import com.ecommerce.challenge.rest.model.CartEntry;
import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.repository.CartEntryRepository;
import com.ecommerce.challenge.rest.repository.CartRepository;
import com.ecommerce.challenge.rest.repository.ProductRepository;
import com.ecommerce.challenge.rest.service.CartService;
import com.ecommerce.challenge.rest.service.impl.DefaultCartService;
import com.ecommerce.challenge.rest.service.impl.DefaultProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(CartController.class)
public class CartControllerIntegrationTest {

    private static final String EXISTING_CART_CODE = "100";
    private static final String NOT_EXISTING_CART_CODE = "900";
    private static final long EXISTING_CART_ENTRY_CODE = 1L;
    private static final long NOT_EXISTING_CART_ENTRY_CODE = 10L;
    private static final String EXISTING_PRODUCT_CODE = "P000001";
    private static final String NOT_EXISTING_PRODUCT_CODE = "H001001";
    private static final String NOT_EXISTING_CART_ERROR_MESSAGE = String.format(DefaultCartService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_CART_CODE);
    private static final String NULL_CART_ERROR_MESSAGE = String.format(DefaultCartService.ENTITY_NOT_FOUND_MESSAGE, null);
    private static final String NOT_EXISTING_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);
    private static final String NULL_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);

    private static Cart existingCart;
    private static CartEntry existingEntry;
    private static Product existingProduct;
    private static Product notExistingProduct;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DefaultProductService productService;

    @MockBean
    private CartService cartService;

    @MockBean
    private ProductRepository productRepository;


    @MockBean
    private CartRepository cartRepository;

    @MockBean
    private CartEntryRepository cartEntryRepository;


    @BeforeAll
    public static void setUp() {
        existingProduct = new Product(EXISTING_PRODUCT_CODE, "", "");
        notExistingProduct = new Product(NOT_EXISTING_PRODUCT_CODE, "", "");
        existingEntry = new CartEntry(EXISTING_CART_ENTRY_CODE, existingProduct, 1);

        final Set<CartEntry> cartEntrySet = new HashSet<>();
        cartEntrySet.add(existingEntry);

        existingCart = new Cart(EXISTING_CART_CODE, cartEntrySet);
    }

    @Test
    void whenGetExistingCartByCode_thenReturnTheCart() throws Exception {
        when(cartService.findCartByCode(EXISTING_CART_CODE))
                .thenReturn(existingCart);

        mockMvc.perform(get("/v1/carts/{cartCode}", EXISTING_CART_CODE))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(EXISTING_CART_CODE)));
    }

    @Test
    void whenGetNotExistingCartByCode_thenThrowsException() throws Exception {
        when(cartService.findCartByCode(NOT_EXISTING_CART_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(
                NestedServletException.class,
                () -> mockMvc.perform(get("/v1/carts/{cartCode}", NOT_EXISTING_CART_CODE))
        );

        Assertions.assertTrue(exception.getMessage().contains(NOT_EXISTING_CART_ERROR_MESSAGE));
    }

    @Test
    void whenGetEntriesForExistingCart_thenReturnTheCartEntries() throws Exception {
        when(cartService.findCartEntriesByCartCode(EXISTING_CART_CODE))
                .thenReturn(existingCart.getEntries());

        mockMvc.perform(get("/v1/carts/{cartCode}/entries", EXISTING_CART_CODE))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(EXISTING_PRODUCT_CODE)));
    }

    @Test
    void whenGetEntriesForNotExistingCart_thenThrowsException() throws Exception {
        when(cartService.findCartEntriesByCartCode(NOT_EXISTING_CART_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(
                NestedServletException.class,
                () -> mockMvc.perform(get("/v1/carts/{cartCode}/entries", NOT_EXISTING_CART_CODE))
        );

        Assertions.assertTrue(exception.getMessage().contains(NOT_EXISTING_CART_ERROR_MESSAGE));
    }

    @Test
    void whenGetProductsForExistingCart_thenReturnProductList() throws Exception {
        when(cartService.findCartEntriesByCartCode(EXISTING_CART_CODE))
                .thenReturn(existingCart.getEntries());

        mockMvc.perform(get("/v1/carts/{cartCode}/products", EXISTING_CART_CODE))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(EXISTING_PRODUCT_CODE)));
    }

    @Test
    void whenGetProductsForNotExistingCart_thenThrowsException() throws Exception {
        when(cartService.findCartEntriesByCartCode(NOT_EXISTING_CART_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(
                NestedServletException.class,
                () -> mockMvc.perform(get("/v1/carts/{cartCode}/products", NOT_EXISTING_CART_CODE))
        );

        Assertions.assertTrue(exception.getMessage().contains(NOT_EXISTING_CART_ERROR_MESSAGE));
    }

    @Test
    void whenAddExistingProductAndExistingEntryProductToCart_thenProductQuantityIsAddedToTheCartEntry() throws Exception {
        final int quantityToAdd = 1;
        final int initialQuantity = existingEntry.getQuantity();

        doAnswer(invocation -> {
            existingEntry.setQuantity(existingEntry.getQuantity() + quantityToAdd);
            return null;
        }).when(cartService).addProductToCart(EXISTING_CART_CODE, EXISTING_PRODUCT_CODE, quantityToAdd);

        mockMvc.perform(
                post("/v1/carts/{cartCode}", EXISTING_CART_CODE)
                        .param("productCode", EXISTING_PRODUCT_CODE)
                        .param("quantity", String.valueOf(quantityToAdd)))
                .andExpect(status().isCreated());

        Assertions.assertEquals(initialQuantity + quantityToAdd, existingEntry.getQuantity());


    }

    @Test
    void whenAddNotExistingProductAndExistingEntryProductToCart_thenThrowsException() throws Exception {
        doThrow(new EntityNotFoundException(NOT_EXISTING_PRODUCT_ERROR_MESSAGE))
                .when(cartService).addProductToCart(EXISTING_CART_CODE, NOT_EXISTING_PRODUCT_CODE, 1);

        Throwable exception = Assertions.assertThrows(
                NestedServletException.class,
                () -> mockMvc.perform(
                        post("/v1/carts/{cartCode}", EXISTING_CART_CODE)
                                .param("productCode", NOT_EXISTING_PRODUCT_CODE)
                                .param("quantity", "1")
                )
        );

        Assertions.assertTrue(exception.getMessage().contains(NOT_EXISTING_PRODUCT_ERROR_MESSAGE));
    }

    @Test
    void whenAddExistingProductAndExistingEntryProductToNotExistingCart_thenThrowsException() throws Exception {
        doThrow(new EntityNotFoundException(NOT_EXISTING_CART_ERROR_MESSAGE))
                .when(cartService).addProductToCart(NOT_EXISTING_CART_CODE, EXISTING_PRODUCT_CODE, 1);

        Throwable exception = Assertions.assertThrows(
                NestedServletException.class,
                () -> mockMvc.perform(
                        post("/v1/carts/{cartCode}", NOT_EXISTING_CART_CODE)
                                .param("productCode", EXISTING_PRODUCT_CODE)
                                .param("quantity", "1")
                )
        );

        Assertions.assertTrue(exception.getMessage().contains(NOT_EXISTING_CART_ERROR_MESSAGE));
    }

}
