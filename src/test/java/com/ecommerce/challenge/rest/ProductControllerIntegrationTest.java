package com.ecommerce.challenge.rest;

import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.repository.CartEntryRepository;
import com.ecommerce.challenge.rest.repository.CartRepository;
import com.ecommerce.challenge.rest.repository.ProductRepository;
import com.ecommerce.challenge.rest.service.CartService;
import com.ecommerce.challenge.rest.service.impl.DefaultProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(ProductController.class)
public class ProductControllerIntegrationTest {

    private static final String EXISTING_PRODUCT_CODE = "P000001";
    private static final String NOT_EXISTING_PRODUCT_CODE = "H001001";
    private static final String NOT_EXISTING_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);
    private static final String NULL_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);


    private static Product existingProduct;
    private static List<Product> productList;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DefaultProductService productService;

    @MockBean
    private ProductRepository productRepository;

    @MockBean
    private CartService cartService;

    @MockBean
    private CartRepository cartRepository;

    @MockBean
    private CartEntryRepository cartEntryRepository;


    @BeforeAll
    public static void setUp() {
        existingProduct = new Product(EXISTING_PRODUCT_CODE, "", "");

        productList = new ArrayList<>();
        productList.add(existingProduct);
        productList.add(new Product("P000002", "", ""));
        productList.add(new Product("P000003", "", ""));
        productList.add(new Product("P000004", "", ""));
        productList.add(new Product("P000005", "", ""));
    }

    @Test
    void whenFindAllProducts_thenReturnTheListWithAllProducts() throws Exception {
        when(productService.findAllProducts())
                .thenReturn(productList);

        mockMvc.perform(get("/v1/products"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(EXISTING_PRODUCT_CODE)));
    }

    @Test
    void whenFindExistingProduct_thenReturnProduct() throws Exception {
        when(productService.findProductByCode(EXISTING_PRODUCT_CODE))
                .thenReturn(existingProduct);

        mockMvc.perform(get("/v1/products/{productCode}", EXISTING_PRODUCT_CODE))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(EXISTING_PRODUCT_CODE)));
    }

    @Test
    void whenFindNotExistingProduct_thenThrowsException() throws Exception {
        when(productService.findProductByCode(NOT_EXISTING_PRODUCT_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_PRODUCT_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(
                NestedServletException.class,
                () -> mockMvc.perform(get("/v1/products/{productCode}", NOT_EXISTING_PRODUCT_CODE))
        );

        Assertions.assertTrue(exception.getMessage().contains(NOT_EXISTING_PRODUCT_ERROR_MESSAGE));

    }

}
