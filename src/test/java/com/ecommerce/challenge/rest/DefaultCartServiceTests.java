package com.ecommerce.challenge.rest;

import com.ecommerce.challenge.rest.model.Cart;
import com.ecommerce.challenge.rest.model.CartEntry;
import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.repository.CartEntryRepository;
import com.ecommerce.challenge.rest.repository.CartRepository;
import com.ecommerce.challenge.rest.service.ProductService;
import com.ecommerce.challenge.rest.service.impl.DefaultCartService;
import com.ecommerce.challenge.rest.service.impl.DefaultProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import javax.el.PropertyNotFoundException;
import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.mockito.Mockito.*;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.1.8
 */
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class DefaultCartServiceTests {

    private static final String EXISTING_CART_CODE = "100";
    private static final String NOT_EXISTING_CART_CODE = "900";
    private static final long EXISTING_CART_ENTRY_CODE = 1L;
    private static final long NOT_EXISTING_CART_ENTRY_CODE = 10L;
    private static final String EXISTING_PRODUCT_CODE = "P000001";
    private static final String NOT_EXISTING_PRODUCT_CODE = "H001001";
    private static final String NOT_EXISTING_CART_ERROR_MESSAGE = String.format(DefaultCartService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_CART_CODE);
    private static final String NULL_CART_ERROR_MESSAGE = String.format(DefaultCartService.ENTITY_NOT_FOUND_MESSAGE, null);
    private static final String NOT_EXISTING_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);
    private static final String NULL_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);

    private static Cart existingCart;
    private static CartEntry existingEntry;
    private static Product existingProduct;
    private static Product notExistingProduct;

    @Mock
    private CartRepository cartRepository;

    @Mock
    private CartEntryRepository cartEntryRepository;

    @Mock
    private ProductService productService;

    @InjectMocks
    private DefaultCartService cartService;


    @BeforeAll
    public static void setUp() {
        existingProduct = new Product(EXISTING_PRODUCT_CODE, "", "");
        notExistingProduct = new Product(NOT_EXISTING_PRODUCT_CODE, "", "");
        existingEntry = new CartEntry(EXISTING_CART_ENTRY_CODE, existingProduct, 1);

        final Set<CartEntry> cartEntrySet = new HashSet<>();
        cartEntrySet.add(existingEntry);
        cartEntrySet.add(mock(CartEntry.class));
        cartEntrySet.add(mock(CartEntry.class));
        cartEntrySet.add(mock(CartEntry.class));
        cartEntrySet.add(mock(CartEntry.class));

        existingCart = new Cart(EXISTING_CART_CODE, cartEntrySet);
    }

    @Test
    void whenFindCartByExistingCode_thenReturnTheCart() {
        when(cartRepository.findByCode(EXISTING_CART_CODE))
                .thenReturn(Optional.of(existingCart));

        final Cart cart = cartService.findCartByCode(EXISTING_CART_CODE);
        Assertions.assertEquals(EXISTING_CART_CODE, cart.getCode());
    }

    @Test
    void whenFindCartByNotExistingCode_thenThrowException() {
        when(cartRepository.findByCode(NOT_EXISTING_CART_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.findCartByCode(NOT_EXISTING_CART_CODE));

        Assertions.assertEquals(NOT_EXISTING_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenFindCartByNullCartCode_thenThrowException() {
        when(cartRepository.findByCode(null))
                .thenThrow(new EntityNotFoundException(
                        NULL_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.findCartByCode(null));

        Assertions.assertEquals(NULL_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenFindCartEntriesByExistingCartCode_thenReturnAllEntries() {
        when(cartRepository.findByCode(EXISTING_CART_CODE))
                .thenReturn(Optional.of(existingCart));

        final Set<CartEntry> cartEntries = cartService.findCartEntriesByCartCode(EXISTING_CART_CODE);
        Assertions.assertEquals(existingCart.getEntries().size(), cartEntries.size());
    }

    @Test
    void whenFindCartEntriesByExistingCartCode_thenExistingEntryExists() {
        when(cartRepository.findByCode(EXISTING_CART_CODE))
                .thenReturn(Optional.of(existingCart));

        final Set<CartEntry> cartEntries = cartService.findCartEntriesByCartCode(EXISTING_CART_CODE);

        Assertions.assertTrue(cartEntries.contains(existingEntry));
    }

    @Test
    void whenFindCartEntriesByNotExistingCartCode_thenThrowException() {
        when(cartRepository.findByCode(NOT_EXISTING_CART_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.findCartEntriesByCartCode(NOT_EXISTING_CART_CODE));

        Assertions.assertEquals(NOT_EXISTING_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenFindCartEntriesByNullCartCode_thenThrowException() {
        when(cartRepository.findByCode(null))
                .thenThrow(new EntityNotFoundException(
                        NULL_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.findCartEntriesByCartCode(null));

        Assertions.assertEquals(NULL_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddExistingProductAndExistingEntryProductToCart_thenProductQuantityIsAddedToTheCartEntry() {
        when(cartRepository.findByCode(EXISTING_CART_CODE))
                .thenReturn(Optional.of(existingCart));

        when(cartEntryRepository.findByCartAndProduct(existingCart, existingProduct))
                .thenReturn(Optional.of(existingEntry));

        when(cartEntryRepository.save(any(CartEntry.class))).thenReturn(null);

        when(productService.findProductByCode(EXISTING_PRODUCT_CODE))
                .thenReturn(existingProduct);

        final int initialQuantity = existingEntry.getQuantity();
        final int quantityToIncrement = 1;

        cartService.addProductToCart(EXISTING_CART_CODE, EXISTING_PRODUCT_CODE, quantityToIncrement);
        Assertions.assertEquals(initialQuantity + quantityToIncrement, existingEntry.getQuantity());
    }

    /**
     * Assuming that the "notExistingProduct" actually exists but there are no entry with it in the cart
     */
    @Test
    void whenAddExistingProductAndNotExistingEntryProductToCart_thenProductIsAddedToTheCartAsNewEntry() {
        final CartEntry newEntry = new CartEntry(NOT_EXISTING_CART_ENTRY_CODE, notExistingProduct, 0);

        when(cartRepository.findByCode(EXISTING_CART_CODE))
                .thenReturn(Optional.of(existingCart));

        when(cartEntryRepository.findByCartAndProduct(existingCart, notExistingProduct))
                .thenReturn(Optional.of(newEntry));

        when(productService.findProductByCode(NOT_EXISTING_PRODUCT_CODE))
                .thenReturn(notExistingProduct);

        doAnswer(invocation -> {
            existingCart.getEntries().add(newEntry);
            return newEntry;
        }).when(cartEntryRepository).save(any(CartEntry.class));



        final int initialEntries = existingCart.getEntries().size();
        final int quantityToAdd = 1;

        cartService.addProductToCart(EXISTING_CART_CODE, NOT_EXISTING_PRODUCT_CODE, quantityToAdd);
        Assertions.assertEquals(initialEntries + 1, existingCart.getEntries().size());
        Assertions.assertTrue(existingCart.getEntries().contains(newEntry));
    }

    @Test
    void whenAddNullProductToCart_thenThrowException() {
        when(cartRepository.findByCode(EXISTING_CART_CODE))
                .thenReturn(Optional.of(existingCart));

        when(productService.findProductByCode(null))
                .thenThrow(new EntityNotFoundException(
                        NULL_PRODUCT_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, null, 1));

        Assertions.assertEquals(NULL_PRODUCT_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddNotExistingProductToCart_thenThrowException() {
        when(cartRepository.findByCode(EXISTING_CART_CODE))
                .thenReturn(Optional.of(existingCart));

        when(productService.findProductByCode(NOT_EXISTING_PRODUCT_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_PRODUCT_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, NOT_EXISTING_PRODUCT_CODE, 1));

        Assertions.assertEquals(NOT_EXISTING_PRODUCT_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddExistingProductToNotExistingCart_thenThrowException() {
        when(cartRepository.findByCode(NOT_EXISTING_CART_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.addProductToCart(NOT_EXISTING_CART_CODE, EXISTING_PRODUCT_CODE, 1));

        Assertions.assertEquals(NOT_EXISTING_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddExistingProductToNullCart_thenThrowException() {
        when(cartRepository.findByCode(null))
                .thenThrow(new EntityNotFoundException(
                        NULL_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.addProductToCart(null, EXISTING_PRODUCT_CODE, 1));

        Assertions.assertEquals(NULL_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddExistingProductWith0QuantityToCart_thenProductQuantityIsAddedToTheCartEntry() {
        Throwable exception = Assertions.assertThrows(PropertyNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, EXISTING_PRODUCT_CODE, 0));

        Assertions.assertEquals(DefaultCartService.QUANTITY_NOT_VALID_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddExistingProductWithNegativeQuantityToCart_thenProductQuantityIsAddedToTheCartEntry() {
        Throwable exception = Assertions.assertThrows(PropertyNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, EXISTING_PRODUCT_CODE, -1));

        Assertions.assertEquals(DefaultCartService.QUANTITY_NOT_VALID_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddNotExistingProductToNullCart_thenThrowException() {
        when(cartRepository.findByCode(null))
                .thenThrow(new EntityNotFoundException(
                        NULL_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.addProductToCart(null, NOT_EXISTING_PRODUCT_CODE, 1));

        Assertions.assertEquals(NULL_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddNotExistingProductWith0QuantityToCart_thenProductQuantityIsAddedToTheCartEntry() {
        Throwable exception = Assertions.assertThrows(PropertyNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, NOT_EXISTING_PRODUCT_CODE, 0));

        Assertions.assertEquals(DefaultCartService.QUANTITY_NOT_VALID_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddNotExistingProductWithNegativeQuantityToCart_thenProductQuantityIsAddedToTheCartEntry() {
        Throwable exception = Assertions.assertThrows(PropertyNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, NOT_EXISTING_PRODUCT_CODE, -1));

        Assertions.assertEquals(DefaultCartService.QUANTITY_NOT_VALID_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddNullProductToNullCart_thenThrowException() {
        when(cartRepository.findByCode(null))
                .thenThrow(new EntityNotFoundException(
                        NULL_CART_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                cartService.addProductToCart(null, null, 1));

        Assertions.assertEquals(NULL_CART_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddNullProductWith0QuantityToCart_thenProductQuantityIsAddedToTheCartEntry() {
        Throwable exception = Assertions.assertThrows(PropertyNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, null, 0));

        Assertions.assertEquals(DefaultCartService.QUANTITY_NOT_VALID_MESSAGE, exception.getMessage());
    }

    @Test
    void whenAddNullProductWithNegativeQuantityToCart_thenProductQuantityIsAddedToTheCartEntry() {
        Throwable exception = Assertions.assertThrows(PropertyNotFoundException.class, () ->
                cartService.addProductToCart(EXISTING_CART_CODE, null, -1));

        Assertions.assertEquals(DefaultCartService.QUANTITY_NOT_VALID_MESSAGE, exception.getMessage());
    }
}
