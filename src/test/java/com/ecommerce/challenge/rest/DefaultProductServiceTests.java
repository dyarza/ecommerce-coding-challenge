package com.ecommerce.challenge.rest;

import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.repository.ProductRepository;
import com.ecommerce.challenge.rest.service.impl.DefaultProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.1.7
 */
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
class DefaultProductServiceTests {

    private static final String EXISTING_PRODUCT_CODE = "P000001";
    private static final String NOT_EXISTING_PRODUCT_CODE = "H001001";
    private static final int PRODUCT_LIST_SIZE = 5;
    private static final String NOT_EXISTING_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);
    private static final String NULL_PRODUCT_ERROR_MESSAGE = String.format(DefaultProductService.ENTITY_NOT_FOUND_MESSAGE, NOT_EXISTING_PRODUCT_CODE);

    private static Product existingProduct;
    private static List<Product> productList;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private DefaultProductService productService;


    @BeforeAll
    public static void setUp() {
        existingProduct = new Product(EXISTING_PRODUCT_CODE, "", "");

        productList = mock(List.class);
        when(productList.get(0)).thenReturn(existingProduct);
        when(productList.size()).thenReturn(PRODUCT_LIST_SIZE);
        when(productList.contains(existingProduct)).thenReturn(true);
    }

    @Test
    void whenFindAllProducts_thenReturnTheListWithAllProducts() {
        when(productRepository.findAll())
                .thenReturn(productList);

        final List<Product> resultProductList = productService.findAllProducts();

        Assertions.assertEquals(productList.size(), resultProductList.size());
        Assertions.assertTrue(productList.contains(existingProduct));
    }

    @Test
    void whenFindAllProducts_thenExistingProductExists() {
        when(productRepository.findAll())
                .thenReturn(productList);

        final List<Product> resultProductList = productService.findAllProducts();

        Assertions.assertTrue(resultProductList.contains(existingProduct));
    }

    @Test
    void whenFindExistingProduct_thenReturnProduct() {
        when(productRepository.findByCode(EXISTING_PRODUCT_CODE))
                .thenReturn(Optional.of(existingProduct));

        Assertions.assertEquals(EXISTING_PRODUCT_CODE,
                productService.findProductByCode(EXISTING_PRODUCT_CODE).getCode());
    }

    @Test
    void whenFindNotExistingProduct_thenThrowException() {
        when(productRepository.findByCode(NOT_EXISTING_PRODUCT_CODE))
                .thenThrow(new EntityNotFoundException(
                        NOT_EXISTING_PRODUCT_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                productService.findProductByCode(NOT_EXISTING_PRODUCT_CODE));

        Assertions.assertEquals(NOT_EXISTING_PRODUCT_ERROR_MESSAGE, exception.getMessage());
    }

    @Test
    void whenFindNullProduct_thenThrowException() {
        when(productRepository.findByCode(null))
                .thenThrow(new EntityNotFoundException(
                        NULL_PRODUCT_ERROR_MESSAGE));

        Throwable exception = Assertions.assertThrows(EntityNotFoundException.class, () ->
                productService.findProductByCode(null));

        Assertions.assertEquals(NULL_PRODUCT_ERROR_MESSAGE, exception.getMessage());
    }
}
