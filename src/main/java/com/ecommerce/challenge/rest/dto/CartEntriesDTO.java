package com.ecommerce.challenge.rest.dto;

import com.ecommerce.challenge.rest.model.CartEntry;

import java.util.HashSet;
import java.util.Set;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.5
 */
public class CartEntriesDTO {

    private Set<CartEntry> entries;

    public CartEntriesDTO() {
        entries = new HashSet<>();
    }

    public CartEntriesDTO(final Set<CartEntry> entries) {
        this.entries = entries;
    }

    public Set<CartEntry> getEntries() {
        return entries;
    }

    public void setEntries(final Set<CartEntry> entries) {
        this.entries = entries;
    }
}
