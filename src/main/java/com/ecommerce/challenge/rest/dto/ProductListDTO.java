package com.ecommerce.challenge.rest.dto;

import com.ecommerce.challenge.rest.model.Product;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.8
 */
public class ProductListDTO {

    List<Product> products;

    public ProductListDTO() {
        this.products = new ArrayList<>();
    }

    public ProductListDTO(final List<Product> products) {
        this.products = products;
    }

    
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(final List<Product> products) {
        this.products = products;
    }
}
