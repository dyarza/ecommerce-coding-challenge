package com.ecommerce.challenge.rest.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.1
 */
@Entity
public class CartEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(updatable = false, nullable = false)
    private Long entryNumber;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "cart_code", nullable = false)
    @JsonBackReference
    private Cart cart;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "product_code", nullable = false)
    private Product product;

    private int quantity;

    public CartEntry() {
    }

    public CartEntry(final Long entryNumber, final Product product, final int quantity) {
        this.entryNumber = entryNumber;
        this.product = product;
        this.quantity = quantity;
    }

    public CartEntry(final Cart cart, final Product product, final int quantity) {
        this.cart = cart;
        this.product = product;
        this.quantity = quantity;
    }

    public Long getEntryNumber() {
        return entryNumber;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(final Cart cart) {
        this.cart = cart;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(final int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CartEntry{" +
                "entryNumber=" + entryNumber +
                ", cart=" + cart.getCode() +
                ", product=" + product.getCode() +
                ", quantity=" + quantity +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CartEntry cartEntry = (CartEntry) o;
        return cart.equals(cartEntry.cart) && product.equals(cartEntry.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cart, product);
    }
}
