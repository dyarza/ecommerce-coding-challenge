package com.ecommerce.challenge.rest.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.1
 */
@Entity
public class Cart {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String code;

    @OneToMany(mappedBy = "cart", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<CartEntry> entries;


    public Cart() {
        this.entries = new HashSet<>();
    }

    public Cart(final String code, final Set<CartEntry> entries) {
        this.code = code;
        this.entries = entries;
    }

    public String getCode() {
        return code;
    }

    public Set<CartEntry> getEntries() {
        return entries;
    }

    public void setEntries(final Set<CartEntry> entries) {
        this.entries = entries;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "code='" + code + '\'' +
                '}';
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Cart cart = (Cart) o;
        return code.equals(cart.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
