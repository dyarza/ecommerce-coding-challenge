package com.ecommerce.challenge.rest;

import com.ecommerce.challenge.rest.dto.CartEntriesDTO;
import com.ecommerce.challenge.rest.dto.ProductListDTO;
import com.ecommerce.challenge.rest.model.Cart;
import com.ecommerce.challenge.rest.model.CartEntry;
import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.service.CartService;
import com.ecommerce.challenge.rest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.5
 */
@RestController
@RequestMapping("/v1/carts/{cartCode:.*}")
public class CartController {

    @Autowired
    private CartService cartService;


    /**
     * Get the cart object for the given cartCode.
     *
     * @param cartCode The cart code
     * @return The JSON representation for the cart object
     */
    @GetMapping
    @ResponseBody
    public Cart getCartByCode(@PathVariable final String cartCode) {
        return getCartService().findCartByCode(cartCode);
    }

    /**
     * Get all cart entry objects in the cart for the given cartCode.
     *
     * @param cartCode The cart code
     * @return The JSON representation for the cart entry object list
     */
    @GetMapping("/entries")
    @ResponseBody
    public CartEntriesDTO getEntriesForCart(@PathVariable final String cartCode) {
        final Set<CartEntry> cartEntries = getCartService().findCartEntriesByCartCode(cartCode);
        return new CartEntriesDTO(cartEntries);
    }

    /**
     * Get all product objects in the cart for the given cartCode.
     *
     * @param cartCode The cart code
     * @return The JSON representation for the product object list
     */
    @GetMapping("/products")
    @ResponseBody
    public ProductListDTO getProductsForCart(@PathVariable final String cartCode) {
        final Set<CartEntry> cartEntries = getCartService().findCartEntriesByCartCode(cartCode);
        return new ProductListDTO(
                cartEntries.stream()
                        .map(CartEntry::getProduct)
                        .collect(Collectors.toList())
        );
    }

    /**
     * Add the given product to the cart for the given cartCode.
     * If the product already exists in an entry of the cart, the quantity will be added to that entry.
     *
     * @param cartCode The cart code
     * @param productCode The product code to add to the cart
     * @param quantity The quantity to add of the product to the cart
     */
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void addProductToCart(@PathVariable final String cartCode,
                                 @RequestParam(value = "productCode") String productCode,
                                 @RequestParam(value = "quantity") int quantity) {
        getCartService().addProductToCart(cartCode, productCode, quantity);
    }


    private CartService getCartService() {
        return cartService;
    }

}
