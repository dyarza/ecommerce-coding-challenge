package com.ecommerce.challenge.rest;

import com.ecommerce.challenge.rest.dto.ProductListDTO;
import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.8
 */
@RestController
@RequestMapping("/v1/products")
public class ProductController {

    @Autowired
    private ProductService productService;


    /**
     * Get the product object for the given productCode.
     *
     * @param productCode The product code
     * @return The JSON representation for the product object
     */
    @GetMapping("/{productCode:.*}")
    @ResponseBody
    public Product getProductByCode(@PathVariable final String productCode) {
        return getProductService().findProductByCode(productCode);
    }

    /**
     * Get all product objects in the system.
     *
     * @return The JSON representation for the product object list
     */
    @GetMapping
    @ResponseBody
    public ProductListDTO getAllProducts() {
        final List<Product> productList = getProductService().findAllProducts();
        return new ProductListDTO(productList);
    }


    private ProductService getProductService() {
        return productService;
    }
}
