package com.ecommerce.challenge.rest;

import com.ecommerce.challenge.rest.model.Cart;
import com.ecommerce.challenge.rest.model.CartEntry;
import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.repository.CartEntryRepository;
import com.ecommerce.challenge.rest.repository.CartRepository;
import com.ecommerce.challenge.rest.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.1
 */
@SpringBootApplication
public class RestApplication {

    private static final Logger LOG = LoggerFactory.getLogger(RestApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RestApplication.class, args);
    }

    @Bean
    public CommandLineRunner demo(ProductRepository productRepository, CartRepository cartRepository,
                                  CartEntryRepository cartEntryRepository) {
        return args -> {

            LOG.info("/*****************************************");
            LOG.info("************* PRODUCTS DEMO **************");
            LOG.info("*****************************************/");
            LOG.info("");

            // save a few products
            final Product productP000001 = new Product("P000001", "Product 000001 Name", "Product 000001 Description");
            final Product productP000002 = new Product("P000002", "Product 000002 Name", "Product 000002 Description");

            productRepository.save(productP000001);
            productRepository.save(productP000002);
            productRepository.save(new Product("P000003", "Product 000003 Name", "Product 000003 Description"));
            productRepository.save(new Product("P000004", "Product 000004 Name", "Product 000004 Description"));
            productRepository.save(new Product("P000005", "Product 000005 Name", "Product 000005 Description"));

            // fetch all products
            LOG.info("Products found with findAll():");
            LOG.info("-------------------------------");
            for (Product product : productRepository.findAll()) {
                LOG.info(product.toString());
            }
            LOG.info("");

            // fetch an individual product by code
            productRepository.findByCode("P000001").ifPresent(product -> {
                LOG.info("Product 'P000001' found:");
                LOG.info("--------------------------------");
                LOG.info(product.toString());
                LOG.info("");
            });

            LOG.info("/*****************************************");
            LOG.info("*************** CART DEMO ****************");
            LOG.info("*****************************************/");
            LOG.info("");

            final Cart cart = new Cart();
            final Cart cart2 = new Cart();
            cartRepository.save(cart);
            cartRepository.save(cart2);

            cartEntryRepository.save(new CartEntry(cart, productP000001, 1));
            cartEntryRepository.save(new CartEntry(cart, productP000002, 5));

            LOG.info("Carts found with findAll():");
            LOG.info("-------------------------------");
            for (Cart cartFound : cartRepository.findAll()) {
                LOG.info(cartFound.toString());
            }
            LOG.info("");

            cartEntryRepository.findByCart(cart).ifPresent(entries -> {
                LOG.info("Found " + entries.size() + " entries for cart '" + cart.getCode() + "' :");
                for (final CartEntry entry : entries) {
                    LOG.info(entry.toString());
                }
                LOG.info("");
            });

        };
    }

}
