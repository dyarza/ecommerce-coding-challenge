package com.ecommerce.challenge.rest.repository;

import com.ecommerce.challenge.rest.model.Cart;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.2
 */
public interface CartRepository extends CrudRepository<Cart, String> {

    Optional<Cart> findByCode(String code);

    List<Cart> findAll();

}
