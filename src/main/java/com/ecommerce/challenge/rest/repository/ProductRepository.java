package com.ecommerce.challenge.rest.repository;

import com.ecommerce.challenge.rest.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.8
 */
public interface ProductRepository extends CrudRepository<Product, String> {

    Optional<Product> findByCode(String code);

    List<Product> findAll();

}
