package com.ecommerce.challenge.rest.repository;

import com.ecommerce.challenge.rest.model.Cart;
import com.ecommerce.challenge.rest.model.CartEntry;
import com.ecommerce.challenge.rest.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.2
 */
public interface CartEntryRepository extends CrudRepository<CartEntry, Long> {

    Optional<List<CartEntry>> findByCart(Cart cart);

    Optional<CartEntry> findByCartAndProduct(Cart cart, Product product);

}
