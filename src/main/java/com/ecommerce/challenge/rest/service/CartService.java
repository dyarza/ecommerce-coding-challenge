package com.ecommerce.challenge.rest.service;

import com.ecommerce.challenge.rest.model.Cart;
import com.ecommerce.challenge.rest.model.CartEntry;
import com.ecommerce.challenge.rest.model.Product;

import javax.el.PropertyNotFoundException;
import javax.persistence.EntityNotFoundException;
import java.util.Set;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.5
 */
public interface CartService {

    /**
     * Find the cart with the given code
     *
     * @param code The cart code
     * @return The cart object if exists, otherwise launch a {@link EntityNotFoundException} exception
     * @throws EntityNotFoundException If the cart with the given code does not exist
     */
    Cart findCartByCode(String code) throws EntityNotFoundException;

    /**
     * Find the cart entries for the cart with the given code
     *
     * @param code The cart code
     * @return The cart entry set if the cart exists, otherwise launch a {@link EntityNotFoundException} exception
     * @throws EntityNotFoundException If the cart with the given code does not exists
     */
    Set<CartEntry> findCartEntriesByCartCode(String code) throws EntityNotFoundException;

    /**
     * Add the product to the cart as a new entry if the product does not exists in the entry set,
     * otherwise add the quantity given to the entry with the given product
     *
     * @param cartCode The cart code to add the product
     * @param productCode The product code to add
     * @param quantity The quantity of the given product to add
     * @throws EntityNotFoundException If the cart with the given code or the product does not exists
     * @throws PropertyNotFoundException If the quantity is lower than or equal 0
     */
    void addProductToCart(String cartCode, String productCode, int quantity) throws EntityNotFoundException, PropertyNotFoundException;

}
