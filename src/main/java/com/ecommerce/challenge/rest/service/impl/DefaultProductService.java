package com.ecommerce.challenge.rest.service.impl;

import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.repository.ProductRepository;
import com.ecommerce.challenge.rest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.8
 */
@Service
public class DefaultProductService implements ProductService {

    public static final String ENTITY_NOT_FOUND_MESSAGE = "The product with code '%s' does not exists";

    @Autowired
    private ProductRepository productRepository;


    @Override
    public List<Product> findAllProducts() {
        return getProductRepository().findAll();
    }

    @Override
    public Product findProductByCode(final String code) throws EntityNotFoundException {
        return getProductRepository().findByCode(code).orElseThrow(() ->
                new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_MESSAGE, code)));
    }


    private ProductRepository getProductRepository() {
        return productRepository;
    }

    public void setProductRepository(final ProductRepository productRepository) {
        this.productRepository = productRepository;
    }
}
