package com.ecommerce.challenge.rest.service;

import com.ecommerce.challenge.rest.model.Product;

import javax.persistence.EntityNotFoundException;
import java.util.List;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.8
 */
public interface ProductService {

    /**
     * Find all products in the system
     *
     * @return A list with all products in the system
     */
    List<Product> findAllProducts();

    /**
     * Find the product for the given code
     *
     * @param code The product code
     * @return The product object if exists, otherwise launch a {@link EntityNotFoundException} exception
     * @throws EntityNotFoundException If a product with the given code does not exist
     */
    Product findProductByCode(String code) throws EntityNotFoundException;

}
