package com.ecommerce.challenge.rest.service.impl;

import com.ecommerce.challenge.rest.model.Cart;
import com.ecommerce.challenge.rest.model.CartEntry;
import com.ecommerce.challenge.rest.model.Product;
import com.ecommerce.challenge.rest.repository.CartEntryRepository;
import com.ecommerce.challenge.rest.repository.CartRepository;
import com.ecommerce.challenge.rest.service.CartService;
import com.ecommerce.challenge.rest.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.el.PropertyNotFoundException;
import javax.persistence.EntityNotFoundException;
import java.util.Set;


/**
 * @author <a href="diego.yarza@seidor.es">Diego Yarza</a>
 * @since v0.0.5
 */
@Service
public class DefaultCartService implements CartService {

    public static final String ENTITY_NOT_FOUND_MESSAGE = "The cart with code '%s' does not exists";
    public static final String QUANTITY_NOT_VALID_MESSAGE = "The quantity should be greater than 0";

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CartEntryRepository cartEntryRepository;

    @Autowired
    private ProductService productService;


    @Override
    public Cart findCartByCode(final String code) throws EntityNotFoundException {
        return getCartRepository().findByCode(code).orElseThrow(() ->
                new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_MESSAGE, code)));
    }

    @Override
    public Set<CartEntry> findCartEntriesByCartCode(final String code) throws EntityNotFoundException {
        final Cart cart = getCartRepository().findByCode(code).orElseThrow(() ->
                new EntityNotFoundException(String.format(ENTITY_NOT_FOUND_MESSAGE, code)));
        return cart.getEntries();
    }

    @Override
    public void addProductToCart(final String cartCode, final String productCode, final int quantity)
            throws EntityNotFoundException, PropertyNotFoundException {
        if (quantity <= 0) {
            throw new PropertyNotFoundException(QUANTITY_NOT_VALID_MESSAGE);
        }

        final Cart cart = findCartByCode(cartCode);
        final Product product = getProductService().findProductByCode(productCode);

        final CartEntry cartEntry = getCartEntryRepository().findByCartAndProduct(cart, product)
                .orElse(new CartEntry(cart, product, 0));

        cartEntry.setQuantity(cartEntry.getQuantity() + quantity);

        getCartEntryRepository().save(cartEntry);
    }


    private CartRepository getCartRepository() {
        return cartRepository;
    }

    private CartEntryRepository getCartEntryRepository() {
        return cartEntryRepository;
    }

    private ProductService getProductService() {
        return productService;
    }
}
