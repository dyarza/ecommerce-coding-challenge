# e-Commerce Coding Challenge



## How to run the application

1. Open the terminal window in the project root folder
2. In a terminal execute the following command for compiling and run the tests:
```
./mvnw clean package
```

3. Then, execute the command to run the application:
```
java -jar target/rest-0.1.8-SNAPSHOT.jar
```

## Initial data

When the application is running, you will see in the terminal some initial data created.
There you can find:
- 1 Cart code without entries
- 1 Cart code with entries
- 5 Product codes

This data will help for manual testing the rest application.

## API Reference

### Get all products

<table>
    <tr>
        <th>Method</th>
        <td>GET</td>
    </tr>
    <tr>
        <th>URL</th>
        <td>http://localhost:8080/v1/products</td>
    </tr>
    <tr>
        <th>Parameters</th>
        <td></td>
    </tr>
    <tr>
        <th>Response</th>
        <td>
            <div>
                <b>Header</b></br>
                200 OK
            </div>
            <div>
                <b>Body</b></br>
<pre>
{
    "products": [
        {
            "code": "&lt;String>",
            "name": "&lt;String>",
            "description": "&lt;String>"
        }
    ]
}
</pre>
            </div>
</td>
    </tr>
</table>

### Get all products of a single cart

<table>
    <tr>
        <th>Method</th>
        <td>GET</td>
    </tr>
    <tr>
        <th>URL</th>
        <td>http://localhost:8080/v1/carts/{cartCode}</td>
    </tr>
    <tr>
        <th>Parameters</th>
        <td></td>
    </tr>
    <tr>
        <th>Response</th>
        <td>
            <div>
                <b>Header</b></br>
                200 OK
            </div>
            <div>
                <b>Body</b></br>
<pre>
{
    "code": "&lt;String>",
    "entries": [
        {
            "entryNumber": &lt;int>,
            "product": {
                "code": "&lt;String>",
                "name": "&lt;String>",
                "description": "&lt;String>"
            },
            "quantity": &lt;int>
        }
    ]
}
</pre>
            </div>
</td>
    </tr>
</table>

### Add a product to a cart

<table>
    <tr>
        <th>Method</th>
        <td>POST</td>
    </tr>
    <tr>
        <th>URL</th>
        <td>http://localhost:8080/v1/carts/{cartCode}</td>
    </tr>
    <tr>
        <th>Parameters</th>
        <td>
            <div>
                <b>productCode:</b> </br>
                The product to add to the cart
            </div>
            <div>
                <b>quantity:</b> </br>
                The quantity of the product to add to the cart
            </div>
        </td>
    </tr>
    <tr>
        <th>Response</th>
        <td>
            <div>
                <b>Header</b></br>
                201 Created
            </div>
            <div>
                <b>Body</b></br>
<pre>
&lt;EMPTY>
</pre>
            </div>
</td>
    </tr>
</table>



